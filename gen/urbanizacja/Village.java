package urbanizacja;

class Village  extends Locality implements NumbersHaver
 {
// Atrybuty:
	private int	StreetNumber;

// Getters & Setters:
	public int	getStreetNumber() { return StreetNumber; }
	public void	setStreetNumber(int w) { StreetNumber = w; }

// Operacje:	

// Realizacje niezaimplementowanych metod interfejsów
	public boolean isNumber(){
		 return false;
	}
};
